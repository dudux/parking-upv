﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Parking
{
    public partial class resultado_altas : Form
    {
        protected classes.Plaza p;

        public resultado_altas(classes.Plaza p)
        {
            this.p = p;
            InitializeComponent();          
        }

        private void resultado_altas_Load(object sender, EventArgs e)
        {
            // I make reference to parent form
            f_altas f = (f_altas)this.Owner;
            label_name.Text = f.get_name();

            label_square.Text = p.get_numero().ToString();
            label_flat.Text = p.get_piso().ToString();                     
        }

                                
       


     
    }
}
