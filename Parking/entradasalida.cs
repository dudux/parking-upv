﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Parking
{
    public partial class entradasalida : Form
    {
        protected classes.aplicacion appli;
        protected classes.Aparcamiento ap;
        ArrayList Vcontratos;
        ArrayList Veventuales;
        //ArrayList Tickets;

        public entradasalida(classes.aplicacion appli)
        {
            InitializeComponent();

            this.appli = appli;
            ap = this.appli.obtenerAparcamiento();
          
            Vcontratos = appli.get_ArrayList_Vcontratos();
            Veventuales = appli.get_ArrayList_Veventual();
           // Tickets = new ArrayList();
        }

        /***                    SALIDA DE VEHICULOS                    ****
         *                   Creación de PagosAlquiler y PagoSalidas
         */
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text == "")
                {
                    MessageBox.Show("Introduce una matricula, Por favor");
                    /* Salir del metodo en c# siendo la vuelta un void */
                    return;
                }
                DialogResult dr = MessageBox.Show("¿Quiere que  salga este Vehiculo del Parking?", "ATENCION!!!", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    string matricula = textBox1.Text;
                    int codigo = appli.SalidaVehiculos(matricula);

                    if (codigo == 0)
                    {
                        MessageBox.Show("Ha salido satisfactoriamente el vehiculo Eventual con Matricula: " + matricula);
                    }
                    else if (codigo == 1)
                    {

                        MessageBox.Show("Ha salido satisfactoriamente el vehiculo ya contratado con Matricula: " + matricula);
                    }
                    else
                    {
                        MessageBox.Show("NO ha salido satisfactoriamente el vehiculo.Posiblemente no exista la matricula.");
                    }                    

                }
                else
                {
                    MessageBox.Show("No  ha salido este Vehiculo del Parking porque Usted ha pulsado NO");
                }

            } // fin try

            catch (NullReferenceException nulo1)
            {
                MessageBox.Show("¿Has introducido una Matrícula? Excepción NullReference", "ATENCION!!!", MessageBoxButtons.YesNo);
                MessageBox.Show(nulo1.ToString());
            }
            catch (Exception nulo)
            {
                MessageBox.Show("Consulte este error con el desarrolador o Administrador del Proyecto");
                MessageBox.Show(nulo.ToString());
            }


            } // fin de la clase



        /***                    ENTRADA DE VEHICULOS                    ****
         *                  SI contrato-----------------> Asigno la plaza contratada anteriormente
         *                  SINO ASIGNO PLAZA CON ALGORITMO Y SE GENERA TICKET CON 
         *                  -horaentrada -matricula  -vehiculo estacionado -nºplaza asignada               
         */
        private void button1_Click(object sender, EventArgs e)
        {
            try{
                if(textBox1.Text == ""){
                    MessageBox.Show("Introduce una matricula, Por favor");
                    /* Salir del metodo en c# siendo la vuelta un void */
                    return;                                  
                }
              
                DialogResult dr = MessageBox.Show("¿Introducir este Vehiculo en el Parking?", "ATENCION!!!", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    
                    string matricula = textBox1.Text;
                    int codigo= this.appli.EntradaVehiculos(matricula);

                    if (codigo == 0)
                    {
                        MessageBox.Show("Ha introducido satisfactoriamente el vehiculo Eventual con Matricula: "+matricula);
                    }
                    else if(codigo == 1){

                        MessageBox.Show("Ha introducido el vehiculo ya contratado con Matricula: " + matricula);
                    }
                    else
                    {
                        MessageBox.Show("NO se ha introducido satisfactoriamente el vehiculo.");
                    }
                }
                else
                {
                    MessageBox.Show("No se ha introducido este Vehiculo en el Parking porque Usted pulsó NO");
                }

            } // fin try

            catch (NullReferenceException nulo1)
            {
                MessageBox.Show("¿Has introducido una Matrícula?", "ATENCION!!!", MessageBoxButtons.YesNo);
                MessageBox.Show(nulo1.ToString());
            }
            catch (Exception nulo)
            {
                MessageBox.Show("Consulte este error con el desarrolador o Administrador del Proyecto");
                MessageBox.Show(nulo.ToString());
            }
        }




/*

        private void entradasalida_FormClosing(object sender, FormClosingEventArgs e)
        {
            try{
            
                classes.Form1 f = (classes.Form1)this.Owner;
                int piso=f.Devuelve_Indice_Combo();
                f.DibujarPlanta(piso+1);
                }
            catch(Exception esce){}

        }
*/

    }
}




/**
string dt;
DateTime date = DateTime.Now;    // display format: 4/25/2008 11:45:44 AM
dt = date.ToLongDateString();          // display format:  Friday, April 25, 2008 
dt = date.ToShortDateString();         // display format:  4/25/2008
dt = date.ToShortTimeString();        // display format: 11:45 AM
dt = date.ToLongTimeString();        // display format:  11:45:44 AM

*/