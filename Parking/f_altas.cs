﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Parking
{
    public partial class f_altas : Form
    {


        protected classes.aplicacion appli;

        public f_altas(classes.aplicacion appli)
        {
            this.appli = appli;
            InitializeComponent();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*        METODO PARA DAR DE ALTA UN CONTRATO                    
         */
        private void button1_Click(object sender, EventArgs e)
        {            
            if (textBox_matricula.Text == "" || textBox_name.Text == "" || textBox_nif.Text == "" || comboBox1.SelectedIndex==-1 )
            {
                MessageBox.Show("Todos los campos son obligatorios.Revise los campos e intente de nuevo");
                return;
            }

            int i=comboBox1.SelectedIndex;
            int precio=0;

            if(i==0)precio=80;
            else if(i==1)precio=90;
            else precio=100;

            try
            {
                string nif = textBox_nif.Text.ToString();
                string nombre = textBox_name.Text.ToString();
                string matricula = textBox_matricula.Text.ToString();

                classes.Plaza p= appli.CrearAltaContrato(nif,nombre,matricula, (double)precio);

                if (p != null)
                {
                    MessageBox.Show("Se ha dado de alta el Contrato correctamente.");
                    Form resultado_altas = new resultado_altas(p);
                    resultado_altas.ShowDialog(this);
                }
                else MessageBox.Show("NO se ha dado de alta el Contrato correctamente.");
            }
            catch (Exception eee)
            {
                MessageBox.Show("No se ha podido dar de alta este Contrato. Vuelva a intentar a crear uno nuevo.");
            }
        }


    }
}
