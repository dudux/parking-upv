﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Parking
{
    public partial class resultado_bajas : Form
    {
        protected classes.Plaza p;
        protected classes.aplicacion apli;

        public resultado_bajas(classes.Plaza p,classes.aplicacion apli)
        {
            this.p = p;
            this.apli = apli;
            InitializeComponent();
        }


        private void resultado_bajas_Load(object sender, EventArgs e)
        {                       
           
            classes.Contrato co = ((classes.Contrato)p.get_contrato());
            label_nombre.Text = p.get_contrato().get_Cliente().get_nombre();
            /* Chapuza por las GUIs */
            p.set_contrato(null);
            p.set_vcontrato(null);

            co.set_Vcontrato(null);
            co.set_plaza(null);
            co.set_PagoAlquiler(null);
            
            label_piso.Text = p.get_piso().ToString();
            label_plaza.Text = p.get_numero().ToString();
        }





    }
}
