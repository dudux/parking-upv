﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



namespace Parking
{

    /*******************     FORMULARIO caja NO CONFUNDIR CON LA CLASE Caja  **************************************/ 
    public partial class caja : Form
    {


        private classes.Caja c;
        protected classes.aplicacion apli;
        

        public caja(classes.aplicacion apli)
        {
           
            this.apli = apli;
            this.c=apli.obtenerAparcamiento().get_Caja();

            InitializeComponent();
                      
            double total_caja=c.get_CajaTotal();           
                      
        }

        private void caja_Load(object sender, EventArgs e)
        {
            double total_eventual = 0.0;
            double total_alquiler = 0.0;
            double total=0.0;

            try
            {
                /* Borrado de listas por seguridad */
                listView_alquiler.Items.Clear();
                listView_eventual.Items.Clear();

                /* Creamos las listas de Items tanto para Alquileres como eventuales */
                ListViewItem [] listaItemAlq = new ListViewItem[c.get_Total_Alquileres()];
                ListViewItem [] listaItemSal = new ListViewItem[c.get_Total_Eventuales()];

                int j = 0; /* contador Pagos_alkiler */
                int k = 0; /* contador Pagos_salida */

                for (int i=0;i < c.get_num_Operacion();i++)   
                {
                    classes.Operacion op = (classes.Operacion)c.get_Operacion(i);

                    /***************         CARGA DE CONTRATOS       **********************************/  
                    if (op.GetType().Name == "Pago_Alquiler") 
                    {
                        /* vehiculo mes piso plaza  importe      */
                        classes.Plaza p = ((classes.Pago_Alquiler)op).get_Contrato().get_plaza();
                        classes.VContrato vc = ((classes.Pago_Alquiler)op).get_Contrato().get_Vcontrato();
                        string mes = ((classes.Pago_Alquiler)op).get_mes();  
                        int piso = p.get_piso();
                        int precio = 0;

                        if (piso == 4) precio = 100;
                        else if (piso == 5) precio = 90;
                        else if (piso == 6) precio = 80;
                        else MessageBox.Show("Parece que hay que revisar el codigo");                      

                        listaItemAlq[j] = new ListViewItem(vc.get_matricula().ToString());
                        listaItemAlq[j].SubItems.Add(mes);
                        listaItemAlq[j].SubItems.Add(p.get_piso().ToString());
                        listaItemAlq[j].SubItems.Add(p.get_numero().ToString());
                        listaItemAlq[j].SubItems.Add(precio.ToString("0.00") + " €");
                        j++;
   
                        total_alquiler += precio;
                                            
                    }
                    /***************         CARGA DE EVENTUALES       *************************/
                    else if (op.GetType().Name == "Pago_Salida")
                    {
                        /* vehiculo   he   diaE hS  diaS totalHoras importe*/
                        DateTime fS = ((classes.Pago_Salida)op).get_HoraSalida();
                        DateTime fE = ((classes.Pago_Salida)op).get_ticket().get_hentrada();
                        //string matricula = ((classes.Pago_Salida)op).get_ticket().get_vevent().get_matricula();
                        
                        
                        string HoraS = fS.ToShortTimeString();
                        string DiaS = fS.ToShortDateString();
                        string HoraE = fE.ToShortTimeString();
                        string DiaE = fE.ToShortDateString();
                        string TotalHoras = ((classes.Pago_Salida)op).get_horas().ToString("0.00");
                        string importe = ((classes.Pago_Salida)op).get_cantidad().ToString("0.00") + " €"; 

                        listaItemSal[k] = new ListViewItem(HoraE);                        
                        listaItemSal[k].SubItems.Add(DiaE);
                        listaItemSal[k].SubItems.Add(HoraS);
                        listaItemSal[k].SubItems.Add(DiaS);
                        listaItemSal[k].SubItems.Add(TotalHoras);
                        listaItemSal[k].SubItems.Add(importe);
                        k++;

                        total_eventual += ((classes.Pago_Salida)op).get_cantidad();


                    }
                    else {
                        MessageBox.Show("Tipo de Operacion no deseada!!");
                    }

                    //total = total + op.get_cantidad();
                    total = total_alquiler + total_eventual;
                          
                   
                } //fin for

                /***************       TOTAL   *********************************/
                label_total_eventual.Text = total_eventual.ToString("0.00") + " €";
                label_total.Text = total.ToString("0.00") + " €";
                label_total_alquiler.Text = total_alquiler.ToString("0.00")+" €";

                listView_alquiler.Items.AddRange(listaItemAlq);
                listView_eventual.Items.AddRange(listaItemSal);

            }//fin try
                catch (Exception exce) {
                MessageBox.Show(exce.ToString());
            }
                                                                            
        }   
    }
}
