﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Piso
    {
        //ATRIBUTOS
        private int numero;
        private string color;
        private int precio;
        
        //Agregacion Plaza_Piso
        private Plaza[] arrayPlaza;

        //CONSTRUCTOR
        public Piso(int n,string c,int p)
        {
            numero = n;
            color = c;
            precio = p;
            arrayPlaza = new Plaza[21];

            //Inicializar los objetos plaza de todos los pisos
            //------------------------------------------------
           
                for (int i = 1; i < 21; i++)
                {
                    arrayPlaza[i] = new Plaza(n, i);
                     
                }
            

        }


        //METODOS PROPIOS
        public void set_numero(int n)
        {
            this.numero = n;    
        }

        public int get_numero()
        {
            return this.numero;
        }



        public void set_precio(int p)
        {
            this.precio = p;
        }

        public int get_precio()
        {
            return this.precio;
        }


        public string get_color()
        {
            return this.color;
        }
        public void set_color(string c)
        {
            this.color = c;
        }

        //METODOS PLAZA
        public void set_plaza(Plaza[] p)
        {
            this.arrayPlaza = p;
        }
        public Plaza get_plaza(int p)
        {
            return this.arrayPlaza[p];
        }

    }
}
