using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Parking.classes     // Quitado este espacio dado: Practica_ISS
{
    public class SistemaCentral:IDisposable
    {
        private StreamReader FicheroOcupacion;
        private StreamReader FicheroContratos;


        private static bool lineaEnBlanco(string s)
        {
            if (s == "")
                return true;
            else return s[0] <= ' ';
        }

        public static SistemaCentral accederFicheros(string NombreDirectorio)
        {
            string NombreOcupacion = NombreDirectorio + "\\Ocupacion.txt";
            string NombreContratos = NombreDirectorio + "\\Contratos.txt";

            if (!File.Exists(NombreOcupacion)|| !File.Exists(NombreContratos))
            {
                return null;
            }
            SistemaCentral sc = new SistemaCentral();
            sc.FicheroOcupacion = File.OpenText(NombreOcupacion);
            sc.FicheroContratos = File.OpenText(NombreContratos);
            return sc;
        }

        public void Dispose()
        {
            FicheroContratos.Close();
            FicheroOcupacion.Close();
        }

        public bool ObtenerOcupacion( out string Matricula, out int HoraEntrada, out int MinEntrada, out int DiaEntrada, out int MesEntrada, out int AnyoEntrada, out int Plaza, out int Piso) {
            Matricula = "";
            HoraEntrada=0;
            MinEntrada=0;
            DiaEntrada=0;
            MesEntrada=0;
            AnyoEntrada=0;
            Plaza=0;
            Piso = 0;
            string s = FicheroOcupacion.ReadLine();
            if (s != null)
            {
                if (lineaEnBlanco(s)) return false;
                else
                {
                    Matricula = s;
                    HoraEntrada = int.Parse(FicheroOcupacion.ReadLine());
                    MinEntrada = int.Parse(FicheroOcupacion.ReadLine());
                    DiaEntrada = int.Parse(FicheroOcupacion.ReadLine());
                    MesEntrada = int.Parse(FicheroOcupacion.ReadLine());
                    AnyoEntrada = int.Parse(FicheroOcupacion.ReadLine());
                    Plaza = int.Parse(FicheroOcupacion.ReadLine());
                    Piso = int.Parse(FicheroOcupacion.ReadLine());
                    FicheroOcupacion.ReadLine(); // Consumimos la l�nea en blanco intermedia
                    return true;
                }
            }
            else return false;
        }

        public bool ObtenerContratos( out string Matricula, out int DiaInicio, out int MesInicio, out int AnyoInicio, out int Plaza, out int Piso, out string Nombre, out string DNI){
            Matricula = "" ;
            DiaInicio=0;
            MesInicio=0;
            AnyoInicio=0;
            Plaza = 0;
            Piso = 0;
		 Nombre = "";
            DNI = "";
            string s = FicheroContratos.ReadLine();
            if (s != null)
            {
                if (lineaEnBlanco(s)) return false;
                else
                {
                    Matricula = s;
                    DiaInicio= int.Parse(FicheroContratos.ReadLine());
                    MesInicio = int.Parse(FicheroContratos.ReadLine());
                    AnyoInicio = int.Parse(FicheroContratos.ReadLine());
                    Plaza = int.Parse(FicheroContratos.ReadLine());
                    Piso = int.Parse(FicheroContratos.ReadLine());
                    Nombre = FicheroContratos.ReadLine();
                    DNI = FicheroContratos.ReadLine();
                    FicheroContratos.ReadLine(); // Consumimos la l�nea en blanco intermedia
                    return true;
                }
            }
            else return false;
        }
       

    }
}
