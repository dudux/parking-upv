﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Pago_Salida:Operacion
    {
        // ATRIBUTOS
        private DateTime hora_salida;
        private DateTime fecha_salida;
        private double totalHoras;

        //Atributos derivados
        private Ticket hora_fecha;

        //Relacion Ticket
        private Ticket ticket_salida;



        // CONSTRUCTOR
        public Pago_Salida(Ticket t, DateTime h, DateTime f, double cant,double totalHoras): base(cant)
        {
            this.ticket_salida = t; // Multiplicidad con restriccion 1 => CONSTRUCTOR

            this.hora_salida = h;
            this.fecha_salida = f;
            this.totalHoras = totalHoras;
        }


        //CONSULTORES
        public DateTime get_HoraSalida() 
        {
            return this.hora_salida;
        }

        public double get_horas()
        {
            return this.totalHoras;
        }

        public DateTime get_FechaSalida()
        {
            return this.fecha_salida;
        }



        //MODIFICADORES
        public void set_Horas(double h)
        {
            this.totalHoras = h;
        }


        public void set_HoraSalida(DateTime hs)
        {
            this.hora_salida = hs;
        }

        public void set_FechaSalida(DateTime fs)
        {
            this.fecha_salida = fs;
        }

        //METODOS TICKET
        public void set_ticket(Ticket t)
        {
            this.ticket_salida = t;
        }
        public Ticket get_ticket()
        {
            return this.ticket_salida;
        }

        //METODOS DATOS DERIVADOS
        public void set_horafecha(Ticket hf)
        {
            this.hora_fecha = hf;
        }
        public Ticket get_horafecha()
        {
            return this.hora_fecha;
        }




    }
}
