﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class VEventual:Vehiculo
    {
        //Relacion Veventual-Ticket-Plaza
        private Ticket tick_event;

        //CONSTRUCTOR
        public VEventual(string m)
            : base(m)
        {         
        }

        //METODOS TICKET
        public void set_ticket(Ticket t)
        {
            this.tick_event = t;
        }
        public Ticket get_ticket()
        {
            return this.tick_event;
        }
    }
}
