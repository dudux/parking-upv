﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class VContrato:Vehiculo
    {
        //ATRIBUTOS
        
        // Relacion Vontrato-plaza
        private Plaza pla;
        // Relacion Contrato-Vcontrato-plaza
        private Contrato c;

        //CONSTRUCTOR
        public VContrato(string m)
            : base(m)
        {
           
        }

        //METODOS CONTRATO
        public void set_contrato(Contrato con)
        {
            this.c = con;
        }
        public Contrato get_contrato()
        {
            return this.c;
        }


        //METODOS PLAZA
        public Plaza get_plaza()
        {
            return this.pla;
        }

        public void set_plaza(Plaza p)
        {
            this.pla = p;           
        }
    }
}
