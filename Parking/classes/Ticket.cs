﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Ticket
    {
        //ATRIBUTOS
        private DateTime fecha_entrada;
        private DateTime hora_entrada;

        //Relacion ticket-Veventual-Plaza
        private VEventual vevent_t;
        private Plaza plaza_t;
       
        //Relacion  ticket-Pagosalida
        private Pago_Salida pagosalida_t;


        //CONSTRUCTOR
        public Ticket(DateTime fe,DateTime he)
        {
            this.fecha_entrada=fe;
            this.hora_entrada = he;
            
        }

        //METODOS 
        public void set_fentrada(DateTime fe)
        {
            this.fecha_entrada = fe;
        }
        public DateTime get_fentrada()
        {
            return this.fecha_entrada;
        }

        public void set_hentrada(DateTime he)
        {
            this.hora_entrada = he;
        }
        public DateTime get_hentrada()
        {
            return this.hora_entrada;
        }

        //METODOS VEVENTUAL
        public VEventual get_vevent()
        {
            return this.vevent_t;
        }
        public void set_vevent(VEventual v)
        {
            this.vevent_t = v;
        }

        //METODOS PLAZA
        public void set_plaza(Plaza p)
        {
            this.plaza_t = p;
        }
        public Plaza get_plaza()
        {
            return plaza_t;
        }

        //METODOS PAGOSALIDA
        public void set_pagosal(Pago_Salida ps)
        {
            this.pagosalida_t = ps;
        }
        public Pago_Salida get_pagosal()
        {
            return this.pagosalida_t;
        }


    }
}
