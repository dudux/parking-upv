﻿using System;
using System.Collections; // .Generic quitado para aceptar arraylist
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Contrato
    {
        // ATRIBUTOS
        private DateTime fecha_inicio;
        private Cliente cli;

        // Relacion Vcontrato-plaza-contrato
        private Plaza plz;
        private VContrato vcon;

        // Relacion pagoalquiler-contrato
        private ArrayList ListaPalquiler;


        // CONSTRUCTOR
        public Contrato(DateTime f_ini)
        {
            this.fecha_inicio = f_ini;        
            
            ListaPalquiler = new ArrayList();
        }

       
        // METODOS
        public void set_fechainicio(DateTime f)
        {
            this.fecha_inicio = f;        
        }
        public DateTime get_fechainicio()
        {
            return this.fecha_inicio;
        }

        //METODOS PLAZA pla
        public void set_plaza(Plaza p)
        {
            this.plz = p;
        }

        public Plaza get_plaza()
        {
            return this.plz;
        }


        //METODOS VCONTRATO vcon
        public void set_Vcontrato(VContrato c)
        {
            this.vcon = c;
        }

        public VContrato get_Vcontrato()
        {
            return this.vcon;
        }


        //METODO PAGOALQUILER
        public void set_PagoAlquiler(Pago_Alquiler p)
        {
            this.ListaPalquiler.Add(p);
        }

        public ArrayList get_PagoAlquiler()
        {
            return this.ListaPalquiler;
        }

        public void delete_PagoAlquiler(Pago_Alquiler p)
        {
            this.ListaPalquiler.Remove(p);
        }

        public Cliente get_Cliente()
        {
            return this.cli;
        }

        public void set_Cliente(Cliente cli)
        {
            this.cli = cli;
        }


    }

}
