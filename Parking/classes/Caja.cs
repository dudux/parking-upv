﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    
    public class Caja
    {
        //ATRIBUTOS
        private double total;
        
        //Relacion Operacion-caja
        private ArrayList lista_op;
   

        //CONSTRUCTOR
        public Caja()
        {
            lista_op = new ArrayList();
        }

        //METODOS PROPIOS
        public double get_CajaTotal()
        {
            return this.total;        
        }

        public void set_CajaTotal(double x)
        {
            this.total = this.total + x;
        }


        //METODOS OPERACION        
        public void anyadir_Operacion(Operacion x)
        {
            lista_op.Add(x);
        }

        public void remover_Operacion(Operacion x)
        {
            lista_op.Remove(x);
        }
        public Operacion get_Operacion(int i)
        {
            if (i >= lista_op.Count)
                return null;
            else
                return (Operacion) this.lista_op[i];
        }

        public int get_num_Operacion()
        {
            return lista_op.Count;
        }


        public int get_Total_Alquileres()
        {
            int total = 0;

            for (int i = 0; i < lista_op.Count; i++)
                if (lista_op[i].GetType().Name == "Pago_Alquiler") total++;
            
            return total;
        }


        public int get_Total_Eventuales()
        {
            int total = 0;

            for (int i = 0; i < lista_op.Count; i++)
                if (lista_op[i].GetType().Name == "Pago_Salida") total++;
            return total;
        }
   
    }
}
