﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Operacion
    {
        //ATRIBUTOS
        private double cantidad_cobrada;
   
        //CONSTRUCTOR
        public Operacion(double ca)
        {
            this.cantidad_cobrada = ca;
        }

        //METODOS 
        public void set_cantidad(double c)
        {
            this.cantidad_cobrada = c;
        }
        public double get_cantidad()
        {
            return this.cantidad_cobrada;
        }


    }
}
