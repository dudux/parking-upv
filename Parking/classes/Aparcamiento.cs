﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Aparcamiento
    {
        //ATRIBUTOS
        private string nombre;
        private string direccion;

        //Agregacion Caja-Aparcamiento
        private Caja caja;   

        //Agregacion Piso-Aparcamiento
        private Piso[] pisos;

        //Arrays con los datos de los pisos (precio y color)
        private Array[] Acolores;
        private Array[] Aprecios;

        // CONSTRUCTOR
        public Aparcamiento(Caja c,String n,String d)
        {
            nombre = n;
            direccion = d;
            caja = c;
            pisos = new Piso[7];  

            string[] Acolores = new string[] { "vacio","rojo", "amarillo", "verde", "azul", "morado", "naranja" };
            int[] Aprecios = new int[] { 00,100, 90, 80, 70, 70, 70 };

            //Inicializar los objetos piso del Aparcamiento
            //---------------------------------------------
            for (int i = 1; i <=6 ; i++)
            {
                pisos[i] = new Piso(i, Acolores[i],Aprecios[i]);
            }


        }

        //METODOS PROPIOS
        public string get_nombre()
        {
            return this.nombre;
        }
        public void set_nombre(string n)
        {
            this.nombre = n;
        }

        public string get_dir()
        {
            return this.direccion;
        }
        public void set_dir(string d)
        {
            this.direccion = d;
        }

        //METODOS CAJA
        public void set_CajaTotal(double x)
        {
            this.caja.set_CajaTotal(x);
        }

        public double get_CajaTotal()
        {
            return this.caja.get_CajaTotal();
        }

        public Caja get_Caja()
        {
            return this.caja;
        }

        public void set_Caja(Caja ca)
        {
             this.caja=ca;
        }

        //METODOS  PISOS
        public Piso get_Piso(int piso)
        {
            return this.pisos[piso];
        }

        public void set_Piso(Piso[] new_Piso)
        {
            this.pisos = new_Piso;
        }



        


    }
}
