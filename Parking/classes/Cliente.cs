﻿using System;
using System.Collections; // Quitado  .Generic para que reconozca Arraylist
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Cliente
    {
        // ATRIBUTOS
        private string dni;
        private string nombre;

        private ArrayList listVCon;
        private ArrayList listCon;


        // CONSTRUCTOR
        public Cliente(string n,string d)
        {
            this.nombre=n;
            this.dni=d;
            listVCon = new ArrayList();
            listCon = new ArrayList();

        }



        // METODOS PARA DNI
        public void set_dni(string d)
        {
            this.dni = d;
        }

        public string get_dni()
        {
            return this.dni;
        }

        // METODOS PARA NOMBRE
        public void set_nombre(string n)
        {
            this.nombre = n;
        }
        public string get_nombre()
        {
            return this.nombre;
        }


        //  METODOS PARA CONTRATO
        public void set_contrato(Contrato v)
        {
            this.listCon.Add(v);
        }

        
        public ArrayList get_contrato()
        {

            return this.listCon;
        }

        //  METODOS PARA VCONTRATO
        public void set_Vcontrato(VContrato v)
        {
            this.listVCon.Add(v);
        }


        public ArrayList get_Vcontrato()
        {

            return this.listVCon;
        }        
       



    }
}
