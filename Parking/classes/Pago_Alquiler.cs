﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Pago_Alquiler:Operacion
    {
        // ATRIBUTOS
        private string mes;
        private Contrato ContratoDeAlquiler;

    
        // CONSTRUCTOR
        public Pago_Alquiler(string mes,double cant,Contrato c):base(cant)
        {
            this.mes = mes;
            this.ContratoDeAlquiler = c;
        }

        // METODOS 
        public void set_mes(string m)
        {
            this.mes = m;
        }

        public string get_mes()
        {
            return this.mes;
        }



        // METODOS CONTRATO
        public Contrato get_Contrato()
        {
            return this.ContratoDeAlquiler;
        }
        public void set_Contrato(Contrato c)
        {
            this.ContratoDeAlquiler = c;
        }


    }
}
