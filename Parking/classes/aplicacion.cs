﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Parking.classes
{
    public class aplicacion
    {
        private SistemaCentral sc;
        private Aparcamiento ap;
        private Caja ca;

        //private VEventual ve;
       // private VContrato vc;

        private ArrayList clientes;
        private ArrayList vcontratos;
        private ArrayList contratos;
        private ArrayList veventual;

        protected DateTime HoraDeCargaAplicacion;

        public aplicacion()
        {
            //cargar los 2 ficheros  ocupacion.txt y contratos.txt            
            //La primera vez que se inialice la aplicación  
            vcontratos = new ArrayList();
            contratos = new ArrayList();
            veventual = new ArrayList();
            clientes = new ArrayList();

            this.HoraDeCargaAplicacion= DateTime.Now;
           

            sc = new SistemaCentral();
            ca = new Caja();
            ap = new Aparcamiento(ca,"Parkame","Universidad Politécnica de Valencia");

            this.CargarSistema(Environment.CurrentDirectory);   //"cd ."); 

            double tc = this.CalculaPagosAlquilerAlArrancar(HoraDeCargaAplicacion);
            ca.set_CajaTotal(tc);
        }



        public void LimpiaParking()
        {
            contratos.Clear();
            clientes.Clear();
            veventual.Clear();
            vcontratos.Clear();
            
            for (int i = 1; i < 7; i++)
            {
                for (int j = 1; j < 21; j++)
                {
                    Plaza p=this.ap.get_Piso(i).get_plaza(j);
                    p.set_contrato(null);
                    p.set_vcontrato(null);
                    p.set_ticket(null);
                    
                }
            }
        }

        public Caja get_Caja()
        {
            return this.ca;
        }

        /*
         **
         *  METODOS PARA TRABAJAR CON LA FECHA Y PODER SIMULAR EL FUTURO 
         */ 
        public DateTime get_HoraDeCargaAplicacion()
        {
            return this.HoraDeCargaAplicacion;
        }

        public void set_HoraDeCargaAplicacion(DateTime f_simulada)
        {
            this.HoraDeCargaAplicacion = f_simulada;
        }

        public double CalculaPagosAlquilerAlArrancar(DateTime FechaSistema)
        {
            double tc = 0.0; /*TotalContratos*/
          
            /* Transcurren meses, realizar cobros de alquiler */                        
            for (int i = 0; i < contratos.Count; i++)
            {
                DateTime finicio = ((classes.Contrato)contratos[i]).get_fechainicio();

                int diasC = FechaSistema.Day - finicio.Day;
                int mesC = FechaSistema.Month - finicio.Month;
                int horasC = FechaSistema.Hour - finicio.Hour;

                if (diasC < 0) diasC += 30;
                if (mesC < 0) mesC += 12;
                if (horasC < 0) horasC += 24;

                int mesInicial = finicio.Month;

                int piso = ((classes.Contrato)contratos[i]).get_plaza().get_piso();
                int precio = 0;

                if (piso == 4) precio = 100;
                else if (piso == 5) precio = 90;
                else if (piso == 6) precio = 80;
                else MessageBox.Show("Parece que hay que revisar el codigo"); 

                while(mesC>0)
                {                                      
                    VContrato vc = (VContrato)vcontratos[i];
                    Contrato co = (Contrato)vc.get_contrato();
                    //string mes=co.get_fechainicio().Month.ToString();

                    Pago_Alquiler pa = new Pago_Alquiler(mesInicial.ToString(), (precio * mesC), co);

                    ca.anyadir_Operacion((Operacion)pa);

                    tc += (precio * mesC);
                    //if (mesC > 12) mesC += 12;
                    mesC--;
                    mesInicial++;
                    
                    if (mesInicial > 12) mesInicial -= 12;                                     
                }                
            }
            return tc;
        }

        
        public classes.Cliente get_Cliente(Contrato co)
        {
            return co.get_Cliente();
        }


        public void CargarSistema(string path) 
        {
            this.LimpiaParking();
            // Inicializar colecciones de vehiculos,clientes,contratos.......
            sc = SistemaCentral.accederFicheros(path);

            if(sc!=null)
            {
                string matricula,nombre,dni;
                int hora,dia,mes,min,anyo;
                int plaza,piso;

                /** 4534-ABC              Matricula   (VEHICULO)
                    05                    dia         (CONTRATO)
                    03                    mes         (CONTRATO)
                    2011                  Anyo        (CONTRATO)
                    2                     Plaza       (CONTRATO) 
                    5                     Piso        (CONTRATO)
                    Ana Lopez Martinez    nombre      (CLIENTE)
                    19563562A             DNI         (CLIENTE)
                 */
                
                 while (sc.ObtenerContratos(              
                     out matricula,                   
                     out dia,//hora,
                     out mes,//min,
                     out anyo,
                     out plaza,
                     out piso,
                     out nombre,
                     out dni))
                 {

                     //CREACION DE OBJETOS LEIDOS
                    
                     VContrato vc = new VContrato(matricula);
                     Plaza p = ap.get_Piso(piso).get_plaza(plaza);
                     DateTime fecha = new DateTime(anyo, mes,dia);

                     Cliente cli = new Cliente(nombre, dni);
                     Contrato co = new Contrato(fecha);
                     
                     co.set_plaza(p);
                     co.set_Vcontrato(vc);
                     co.set_Cliente(cli);
                     

                     p.set_contrato(co);
                     p.set_vcontrato(vc);

                     vc.set_plaza(p); 
                     vc.set_contrato(co);

                     

                     vcontratos.Add(vc);
                     contratos.Add(co);
                     clientes.Add(cli);

                    
                
                 } //fin while */

                while (sc.ObtenerOcupacion(
                    out matricula,      //VEventual
                    out hora,           //Ticket 
                    out min,           //Ticket
                    out dia,           //Ticket
                    out mes,           //Ticket
                    out anyo,          //Ticket
                    out plaza,          //Plaza
                    out piso))          //Plaza
                {

                    //buscar si matricula esta en lista de VContratos
                    //si está entonces asigno a la Plaza el vcontrato
                    // al vcontrato le asigno la plaza
                    //si no esta en la lista es veventual y hago lo que sigue a continuacion

                    Plaza p = ap.get_Piso(piso).get_plaza(plaza);  
                    
                    anyo += 2000;
                    DateTime f_ent = new DateTime(anyo, mes, dia, hora, min,00);
                    DateTime f_hora = new DateTime(anyo,mes,dia,hora,min,00);

                    for(int k=0; k< vcontratos.Count;k++)
                    {
                        if (vcontratos[k].Equals(matricula) ){
                            
                            // p.set_vcontrato(vcontratos[k]);
                            // asignar a vcontratos[k]. la plaza
                           
                        }
                        else{

                            VEventual ve = new VEventual(matricula);            
                            Ticket tic = new Ticket(f_ent, f_hora);
                            tic.set_plaza(p);
                            tic.set_vevent(ve);
                            
                            ve.set_ticket(tic);                            
                            p.set_ticket(tic); 
                          
                            veventual.Add(ve);
                          

                        }
                    }                                                    
                    
                } //fin while

            } //fin if
        } //fin carga sistema


       /** public int get_plaza(int pi,int pla)
        {
            return this.ap.get_Piso(pi).get_plaza(pla).get_numero();
        }
        **/

        public int get_piso(int pi)
        {
            return this.ap.get_Piso(pi).get_numero();
        }

        public bool get_contrato(string mat)
        {
            for(int i=1; i<=6;i++){
                for(int j=1;j<=20;j++){
                    if(ap.get_Piso(i).get_plaza(j).get_vcontrato() != null){
                        return true;
                    }
                }
            }
                
            return false;
        }

        public Aparcamiento obtenerAparcamiento()
        {
            return this.ap;
            
        }

        public ArrayList get_plazas_dePiso(int pi)
        {
            ArrayList ocupadas=new ArrayList();

            for (int i = 1; i < 21; i++)
            { 
                if(ap.get_Piso(pi).get_plaza(i).get_vcontrato()!=null)
                {
                    ocupadas.Add(i);
                }               
            }
            return ocupadas;
        }
                          
        /* METODOS PARA ENTRADA-SALIDA QUE RECIBAN LOS ARRAYLISTS */            
        public ArrayList get_ArrayList_Vcontratos()
        {
            return this.vcontratos;
        }

        public ArrayList get_ArrayList_Contratos()
        {
            return this.contratos;
        }

        public ArrayList get_ArrayList_Veventual()
        {
            return this.veventual;
        }

        public ArrayList get_ArrayList_Clientes()
        {
            return this.clientes;
        }

        /* Metodo de entrada de Vehiculos al Parking
         * Entrada : Matricula (string)
         * Salida  : c         (int)
         *           -1        Error
         *            0        Eventual OK!
         *            1        Contrato OK!
         */
        public int EntradaVehiculos(string matricula)
        {
            int c = -1;
            bool encontrado = false;

            /* Indice del Array de Vcontratados de objetos con datos del tipo String(Matriculas) */
            int final = vcontratos.Count;

            /* BUSQUEDA EN EL ARRAYLIST DE VCONTRATADOS PARA VER SI EXISTE ESA MATRICULA   */
            for (int i = 0; i < final && !encontrado; i++)
            {                                             
                if (matricula == ((VContrato) vcontratos[i]).get_matricula())                      
                {
                    classes.VContrato vc = (classes.VContrato)vcontratos[i];                                       
                    classes.Plaza p = vc.get_plaza();                

                    p.set_vcontrato(vc);
                    vc.set_plaza(p); 
                    
                    encontrado = true;
                    c = 1;
                }
            } 

            /*  SINO HEMOS ENCONTRADO ANTERIORMENTE, QUIERE DECIR QUE NO POSEE CONTRATO POR LO TANTO GENERAR TICKET  */
            if (!encontrado)
            {                
                /* Comprobamos si esta dentro del Parking esa Matricula de Eventual */
                bool found = false;
                for (int k = 0; k < veventual.Count && !found; k++)
                {
                    if (matricula == ((VEventual)veventual[k]).get_matricula())
                    {
                        MessageBox.Show("Esa Matricula ya existe en el Parking como Vehiculo Eventual.Primero ha de salir");
                        found = true;
                        return -1;
                    }
                }
                /* Asignar la plaza    */
                classes.Plaza p = AsignaPlazaMasCercana();

                /* Generar Ticket con: Matricula,plaza asignada, horaentrada y vehiculo estacionado   */
                DateTime fecha = DateTime.Now;    // display format: 4/25/2008 11:45:44 AM                    

                Ticket tik = new Ticket(fecha, fecha);
                VEventual ve = new VEventual(matricula);               

                ve.set_matricula(matricula);
                tik.set_plaza(p);
                p.set_ticket(tik);
                tik.set_vevent(ve);                                                 
                ve.set_ticket(tik);

                veventual.Add(ve);
                
                c = 0;                              
            } 
            return c;
        } 



        /* Metodo de Salida de Vehiculos al Parking
         * Entrada : Matricula (string)
         * Salida  :  c        (int)
         *           -1        Error
         *            0        Eventual OK!
         *            1        Contrato OK!
         */
        public int SalidaVehiculos(string matricula)
        {
            bool encontrado = false;
            int codigo = -1;

            /* Indice del Array de Vcontratados de objetos con datos del tipo String(Matriculas) */
            int final = vcontratos.Count;

            /* BUSQUEDA EN EL ARRAYLIST DE VCONTRATADOS PARA VER SI EXISTE ESA MATRICULA   */
            for (int i = 0; i < final && !encontrado; i++)
            {             
                if (matricula == ((VContrato) vcontratos[i]).get_matricula())
                {
                    /* 
                      AQUI PERMITIMOS SALIR, Y DEBEREMOS DEJAR LA PLAZA VACIA PERO "RESERVADA"                   
                     */                    
                    VContrato vc =(VContrato) vcontratos[i];
                    Plaza p=vc.get_plaza();
                  
                   
                    Contrato co = (Contrato)vc.get_contrato();
                    co.set_Vcontrato(vc);

                    //Pago_Alquiler pa = new Pago_Alquiler(matricula, 0, co);
                    
                    //ca.anyadir_Operacion((Operacion)pa);

                    p.set_vcontrato(null);
                    p.set_contrato(co); // ?!
                                                            
                    encontrado = true;
                    codigo = 1;
                }
            } 

            /* GENERAR PAGOSALIDA  ---------->NO POSEE CONTRATO            */
            if (!encontrado)
            {               
                int fin = veventual.Count;
                VEventual ve = null;
                bool enc = false;

                for (int z = 0; z < fin && !enc; z++)
                {
                    if (matricula == ((VEventual)veventual[z]).get_matricula() )  
                    {
                        //Guardo la referencia al objeto encontrado
                        ve=((VEventual)veventual[z]);
                        codigo = 1; // Codigo referencia --> Veventual OK!
                        enc = true;
                    }
                }
                if (!enc) return -1;

                DateTime fecha = DateTime.Now;    // display format: 4/25/2008 11:45:44 AM 
                double cant = 0.0;
                
                Ticket t = ve.get_ticket();               
                Plaza p = t.get_plaza();
                
                //Datetime fe= ve.get_ticket().get_fentrada();
                DateTime he = ve.get_ticket().get_hentrada();
                t.set_fentrada(he);
                t.set_hentrada(he);
                t.set_vevent(ve);
                
                //ve.set_ticket(t);
                //ve.set_matricula(matricula);
                cant = this.CalculaCantidadEventual(he);
                //double horas = (cant / 1.5);

                double hours = this.get_TotalHorasEventual(he, DateTime.Now);
                Pago_Salida ps = new Pago_Salida(t,fecha,he,cant,hours);
                t.set_pagosal(ps);                               
                //ps.set_Horas(horas);
                
                ca.anyadir_Operacion((Operacion) ps);
                                
                //Desreferencio la plaza para liberarla
                t.set_plaza(null);
                ve.set_matricula(null);
                p.set_ticket(null);

            }//fin if_no_encontrado

            return codigo;
        } //fin del metodo salidavehiculos

            
         /*   Metodo que busca la primera plaza libre en el Parking desde la planta baja hacia lo más alto
         *   Entrada:  (void)
         *   Salida :   Plaza
         */
        public classes.Plaza AsignaPlazaMasCercana()
        {
            bool asignada = false;
            classes.Plaza p = null;

            /* comprobar que    asignada = false  <==>  !asignada  */
            for (int piso = 1; piso <= 6 && !asignada; piso++)
            {
                for (int plaza = 1; plaza < 21; plaza++)
                {
                    if (ap.get_Piso(piso).get_plaza(plaza).get_ticket() == null)
                    {
                        p = ap.get_Piso(piso).get_plaza(plaza);
                        asignada = true;
                        break;
                    }
                }
            }
            return p;
        }

        /* Metodo que calcula la cantidad a cobrar a un Vehiculo Eventual
         * Cobrando la     1ª hora--> 1,50€
         *             cada minuto--> 0,025€
         *             
         * Entrada: fechaentrada , horaentrada    (datetime)
         * Salida : CantidadCobrada               (double)
         */         
        public double CalculaCantidadEventual(DateTime fe)
        {
            double total =0.0;
            DateTime fs = DateTime.Now;

            /* Espacio temporal de la FECHA DE ENTRADA   */
            int minE=fe.Minute;
            int diaE = fe.Day;
            int mesE=fe.Month;
            int anyoE=fe.Year;

            /* Espacio temporal de la FECHA DE SALIDA (La del sistema)   */
            int minS = fs.Minute;
            int diaS = fs.Day;
            int mesS = fs.Month;
            int anyoS = fs.Year;

            /* Totales para calculo final  */
            int anyoT = anyoS - anyoE;
            int mesT = mesS - mesE;
            int diaT = diaS - diaE;
            int minT = minS - minE;          

            if (diaT < 0) diaT += 30;
            if (mesT < 0) mesT += 12;
            if (minT < 0) minT += 60;

            MessageBox.Show("Han transcurrido "+anyoT +" años y " + mesT + " meses y " + diaT + " dias y " + minT + " minutos");


            double horas = get_TotalHorasEventual(fe, fs);
            double dias=0;
            double minutos=0;

            if (horas >= 24)
            {
                dias = horas / 24;
                total = dias * 30;
                return total;
            }
            else if (horas <= 1) return total = 1.5;
            else
            {
                minutos = horas * 60;
                total += minutos * 0.025;
                return total;
            }                    
     
                   
        }

        public double get_TotalHorasEventual(DateTime fe, DateTime fs)
        {
            double horas = 0.0;
            TimeSpan inter = fs - fe;
            horas = inter.TotalHours;
            return horas;
        }




        public Plaza    CrearAltaContrato(string nif,string nombre,string matricula,double precio)
        {
            /* Comprobamos si esta dentro del Parking esa Matricula de Contrato */
            bool found = false;
            bool found_eventual = false;

            for (int j = 0; j < veventual.Count && !found_eventual ; j++)
            {
                if (matricula == ((VEventual)veventual[j]).get_matricula())
                {
                    MessageBox.Show("Esa Matricula YA EXISTE en el Parking como Vehiculo Eventual.");
                    found_eventual = true;
                    return null;
                }
            }

            for (int k = 0; k < vcontratos.Count && !found; k++)
            {
                if (matricula == ((VContrato)vcontratos[k]).get_matricula())
                {
                    MessageBox.Show("Esa Matricula YA EXISTE en el Parking como Vehiculo Contratado.");
                    found = true;
                    return null;
                }
            }

            Plaza p = this.BuscaPlazaLibrePorPrecio(precio);

            if (p != null)
            {
                DateTime f = DateTime.Now;
                Contrato co = new Contrato(f);
                VContrato vco = new VContrato(matricula);

                Cliente cl = new Cliente(nombre, nif);


                p.set_contrato(co);
                
                co.set_plaza(p);
                co.set_Vcontrato(vco);
                co.set_Cliente(cl);

                vco.set_contrato(co);
                vco.set_plaza(p);
                vco.set_matricula(matricula); //m,odiff

                p.set_vcontrato(vco);

                /* Añadir al arraylist de vcontratos */
                vcontratos.Add(vco);

                return p;
            }
            else
            {
                MessageBox.Show("Planta llena! Debe esperar a que libere una plaza de Contrato. Perdone las molestias");
                return null;
            }

        }

        /*
         * METODO PARA DAR DE BAJA UN CONTRATO
         *
         */
        public Plaza CrearBajaContrato(string matricula) 
        {
            Plaza p = null;
            bool encontrado = false;

            /* VALIDAR EL CONTRATO */
            for (int i = 0; i < vcontratos.Count && !encontrado; i++)
            {
                if (matricula == ((VContrato)vcontratos[i]).get_matricula())
                {
                    /* DESTRUIR EL CONTRATO */
                    
                    p = ((VContrato)vcontratos[i]).get_plaza();
                    Contrato co = p.get_contrato();


                    int piso = ((VContrato)vcontratos[i]).get_plaza().get_piso();
                   
                    int precio = DamePrecioDePiso(piso);                   
                    
                    string mes = co.get_fechainicio().Month.ToString();

                    Pago_Alquiler pa = new Pago_Alquiler(mes, precio, co);
                    ca.remover_Operacion(pa);
                   
                    // p.set_contrato(null);
                    //p.set_vcontrato(null);

                    ((VContrato)vcontratos[i]).set_plaza(null);                    
                    ((VContrato)vcontratos[i]).set_contrato(null);
                    ((VContrato)vcontratos[i]).set_matricula(null);                                       
                    
                    // co.set_Vcontrato(null);
                    //co.set_plaza(null);
                    //co.set_PagoAlquiler(null);
                    
                    //vcontratos.Remove(matricula);
                    encontrado = true;
                    return p;
                }                
            }
            return null;
        }
        public int DamePrecioDePiso(int piso)
        {
            int precio = 0;

            if (piso == 4) precio = 100;
            else if (piso == 5) precio = 90;
            else if (piso == 6) precio = 80;
            else MessageBox.Show("Parece que hay que revisar el codigo");

            return precio;
        }

        public Plaza BuscaPlazaLibrePorPrecio(double precio)
        {
            int piso=-1;
            Plaza pla = null;
            bool encontrada = false;

            if (precio == 100) piso = 4;
            else if (precio == 90) piso = 5;
            else  piso = 6;

            for (int p = 1; p < 21 && !encontrada; p++){
                if (ap.get_Piso(piso).get_plaza(p).get_vcontrato() == null && ap.get_Piso(piso).get_plaza(p).get_contrato() == null && ap.get_Piso(piso).get_plaza(p).get_ticket() == null)                
                {
                    pla = ap.get_Piso(piso).get_plaza(p);
                    encontrada = true;                    
                }                    
            }            
            return pla;

        }

    }
}

/* FORMATO DE LOS FICHEROS (tipo textual ASCII MS-DOS)
La separación de los campos es por fin de línea.
Por tanto, la descripción de cualquier elemento en un fichero consta
de varias líneas de las cuales contienen (indicamos descripción y tipo):
 * 
OCUPACION:
    string Matricula;
    int HoraEntrada; // entre 0 y 23;
    int MinEntrada; // entre 0 y 59;
    int DiaEntrada; // entre 1 y 31;
    int MesEntrada; //entre 1 y 12
    int AñoEntrada; // desde 2011
    int Plaza; // entre 0 y 20;
    int Piso; // entre 1 y 6;
    LINEA EN BLANCO
 * 
CONTRATOS:
    string Matricula;
    int DiaInicio; // entre 1 y 31;
    int MesInicio; // entre 1 y 12;
    int AnyoInicio; // desde 2011;
    int Plaza; // entre 0 y 20;
    int Piso // entre 3 y 6;
    string Nombre
    string DNI
    LINEA EN BLANCO
*/