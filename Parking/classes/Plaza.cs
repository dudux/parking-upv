﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.classes
{
    public class Plaza
    {
        //ATRIBUTOS
        private int numero;
        private int piso;

        //Relacion Vcontrato-Plaza
        private VContrato vcontrat;
        private Contrato co;

        //Relacion Veventual-ticket-Plaza
        private Ticket ticket_p;

       


        //CONSTRUCTOR
        public Plaza(int p, int n)
        {
            this.piso = p;
            this.numero = n;
           
        }


        //METODOS PROPIOS
        public void set_numero(int n)
        {
            this.numero = n;
        }
        public int get_numero()
        {
            return this.numero;
        }

        public void set_piso(int p)
        {
            this.piso = p;
        }
        public int get_piso()
        {
            return this.piso;
        }


        //METODOS VCONTRATO
        public void set_vcontrato(VContrato vc)
        {
            this.vcontrat=vc;
        }
        public VContrato get_vcontrato()
        {
            return this.vcontrat;
        }
        public void delete_vcontrato(VContrato vc)
        {
            vc=null;
        }


        //METODOS TICKET
        public void set_ticket(Ticket t)
        {
            this.ticket_p = t;
        }
        public Ticket get_ticket()
        {
            return this.ticket_p;
        }

        //METODOS CONTRATO
        public void set_contrato(Contrato c)
        {
            this.co = c;
        }
        public Contrato get_contrato()
        {
            return this.co;
        }
    
    
    
    }

}
