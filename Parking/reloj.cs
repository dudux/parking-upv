﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Parking
{
    public partial class reloj : Form
    {
        protected DateTime fecha;
        protected classes.aplicacion apli;

        public reloj(classes.aplicacion appli)
        {            
            InitializeComponent();
            this.apli=appli;
            ArrayList contratos = apli.get_ArrayList_Contratos();     
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool ValidaFecha(string d, string h, string m)
        {
            try
            {
                int dia = int.Parse(d);
                int hora = int.Parse(h);
                int mes = int.Parse(m);
                

                if (dia <= 0 || dia > 30) return false;
                if (hora < 0 || hora >= 24) return false;
                if (mes <= 0 || mes > 12) return false;
            }
            catch (Exception ee)
            {
                MessageBox.Show("Introduzca números válidos ,porfavor");
                return false;
            }
            return true;
        }

        /* BOTON ACTUALIZAR */
        private void button1_Click(object sender, EventArgs e)
        {
            bool correcto = false;

            string dia=textBox_dia.Text;          
            string hora=textBox_hora.Text;
            string mes=textBox_mes.Text;
                    

            if (dia ==""|| mes ==""|| hora=="")
            { 
                MessageBox.Show("Rellene todos los campos por favor");
                return;
            }

            correcto=ValidaFecha(dia, hora, mes);
            if (!correcto)
            {
                MessageBox.Show("Recuerde dia [1-30] , mes [1-12] , hora [00-24]");
                return;
            }
            else
            {
                DialogResult dr= MessageBox.Show("¿Esta todo correcto?", "ATENCION!!!", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    /* Realizar cobros mensuales */
                    double totalC=SimularReloj(dia, mes, hora);
                    MessageBox.Show("Se han ingresado " + totalC + " € en el Parking de contratos cobrados");
                }
                else return;

            }           
        }

        /*  
         * Escenario 8: Simular Reloj
         * Este escenario permite simular el transcurso del tiempo durante la ejecución del programa.
         *  1. Se modifica la fecha y hora del sistema (introduciendo una nueva fecha o adelantando el
         *     reloj en incrementos de 1 hora y/o 1 día).
         * 2. Se calcula si se ha pasado a un nuevo mes, si es así:
         *      -el sistema automáticamente procede a cobrar a los clientes con contrato.
         */
        public double SimularReloj(string d, string m, string h)
        {
            classes.Caja ca = apli.get_Caja();
            double tc = 0.0; /*TotalContratos*/
            int anyo = 2011;
            int meses = 0;

            int dia = int.Parse(d);
            int hora = int.Parse(h);
            int mes = int.Parse(m);

        
            DateTime f_ahora = DateTime.Now;

            /* Calculo de la fecha simulada */
            meses = (mes - f_ahora.Month);
            if (meses < 0)
            {
                int mesT = (meses + 12);
                /*meses = (meses * (-1));
                anyo = meses / 12;*/
                if (mesT <= 12) anyo++;                
            }
            else anyo = 2011;
            
                DateTime f_simulada = new DateTime(anyo, mes, dia, hora, 0, 0);
            

            
            apli.set_HoraDeCargaAplicacion(f_simulada);

            /* Transcurren meses, realizar cobros de alquiler */           
                
                ArrayList contratos= apli.get_ArrayList_Contratos();
                               
                for (int i = 0; i < contratos.Count; i++)
                {
                    //DateTime finicio = ((classes.Contrato)contratos[i]).get_fechainicio();

                    int diasC = (f_simulada.Day - f_ahora.Day);
                    int mesC = (f_simulada.Month - f_ahora.Month);
                    int horasC = (f_simulada.Hour - f_ahora.Hour);

                    if (diasC < 0) diasC += 30;
                    if (mesC < 0) mesC += 12;
                    if (horasC < 0) horasC += 24;
                    
                    if(i==0)MessageBox.Show("Han transcurrido " + mesC + " meses y " + diasC + " dias y " + horasC + " horas");

                    int mesActual = f_ahora.Month;
                    while(mesC > 0)
                    {
                        int piso = ((classes.Contrato)contratos[i]).get_plaza().get_piso();
                        int precio = 0;

                        if (piso == 4) precio = 100;
                        else if (piso == 5) precio = 90;
                        else if (piso == 6) precio = 80;
                        else MessageBox.Show("Parece que hay que revisar el codigo");
                        classes.Contrato co=(classes.Contrato)contratos[i];
                        classes.Pago_Alquiler pa = new Parking.classes.Pago_Alquiler(mesActual.ToString(), precio, co);
                        ca.anyadir_Operacion((classes.Operacion)pa);

                        tc += precio;
                        mesActual++;
                        mesC--;
                        if (mesActual > 12) mesActual -= 12;
                    }
                }
           
            return tc;


        }

        private void reloj_Load(object sender, EventArgs e)
        {
            DateTime fecha = DateTime.Now;

            textBox_dia.Text = fecha.Day.ToString();
            textBox_hora.Text = fecha.Hour.ToString();
            textBox_mes.Text = fecha.Month.ToString();    

        }

        /* Cobrar alquileres a dia de hoy  */
        private void button3_Click(object sender, EventArgs e)
        {
            /* Realizar cobros mensuales a fecha del sistema */
            DateTime f=DateTime.Now;
            double totalC = CobroMensualADiaDeHoy(f);
            MessageBox.Show("El cobro de todos los contratos a dia de hoy : "+DateTime.Now.ToShortDateString() +" asciende a : " + totalC+" €");

        }

        /* SI COBRO TODOS LOS CONTRATOS,DEBERE DE CAMBIAR LA FECHAINICIO O ALGO PARA SABER QUE ESTAN COBRADOS*/
        public double CobroMensualADiaDeHoy(DateTime f)
        {
            double tc = 0.0; /*TotalContratos*/

            int dia = f.Day;
            int hora = f.Hour;
            int mes = f.Month;
            // MessageBox.Show("Han transcurrido "+meses+" meses y "  +dias+" dias y "+horas+" horas");
                            
            ArrayList contratos= apli.get_ArrayList_Contratos();
                               
            for (int i = 0; i < contratos.Count; i++)
            {
                    DateTime finicio = ((classes.Contrato)contratos[i]).get_fechainicio();
                    int diasC=f.Day - finicio.Day;
                    int mesC = f.Month - finicio.Month;
                    int anyoC = f.Year - finicio.Year;

                    if (diasC < 0) diasC += 30;
                    if (mesC < 0) mesC += 12;
                    

                    int piso=((classes.Contrato)contratos[i]).get_plaza().get_piso();
                    int precio = 0;

                    if (piso == 4) precio = 100;
                    else if (piso == 5) precio = 90;
                    else if (piso == 6) precio = 80;
                    else MessageBox.Show("Parece que hay que revisar el codigo");

                    tc += (precio * mesC);
             }                
        
            return tc;

        }


    }
}
