﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace Parking.classes
{
    public partial class Form1 : Form
    {
        protected Caja ca;
        protected aplicacion appli;
        protected Aparcamiento ap;
        protected ArrayList vc;
        protected ArrayList ve;
        

        public Form1(aplicacion appli)
        {
            InitializeComponent();
            this.appli = appli;
            ap = this.appli.obtenerAparcamiento();



            // OCULTAR PANEL Y LABELS DE LAS PLANTAS CON LA CARGA
            panel1.Visible = false;
            label1_npanta_value.Visible = false;

            Inicializa_plazas_form();
        }

        public int Devuelve_Indice_Combo()
        {
            return (int)this.comboBox1.SelectedIndex;
        }

        public void Inicializa_plazas_form()
        {
            //10,10    10,66   10,123   10,180
            // Create control panel with twenty pictures                      
            int x = 10, y = 10;


            for (int plaza = 1; plaza <= 20; plaza++)
            {
                PictureBox p = new PictureBox();
                switch (plaza)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        p.Size = new System.Drawing.Size(70, 36);
                        p.Location = new System.Drawing.Point(x + 4 * (plaza) * 18, y);
                        break;

                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        x = 10; y = 66;
                        p.Size = new System.Drawing.Size(70, 36);
                        p.Location = new System.Drawing.Point(x + 4 * (plaza - 5) * 18, y);
                        break;

                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                        x = 10; y = 123;
                        p.Size = new System.Drawing.Size(70, 36);
                        p.Location = new System.Drawing.Point(x + 4 * (plaza - 10) * 18, y);
                        break;

                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                        x = 10; y = 180;
                        p.Size = new System.Drawing.Size(70, 36);
                        p.Location = new System.Drawing.Point(x + 4 * (plaza - 15) * 18, y);
                        break;

                } //fin switch        
                panel1.Controls.Add(p);

            }
        }

        public void DibujarPlanta(int piso)
        {

            PictureBox p;
            this.toolTip1.RemoveAll();

            for (int plaza = 1; plaza <= 20; plaza++)
            {

                p = (PictureBox)panel1.Controls[plaza - 1];

                    /* Reservada */
                    if (ap.get_Piso(piso).get_plaza(plaza).get_contrato() != null && ap.get_Piso(piso).get_plaza(plaza).get_vcontrato() == null)
                    {
                        p.Image = global::Parking.Properties.Resources.plaza_reservada01;
                        this.toolTip1.SetToolTip(p, "Piso:" + piso + "\nPlaza: " + plaza+"\nMatricula: "+ap.get_Piso(piso).get_plaza(plaza).get_contrato().get_Vcontrato().get_matricula());                        
                    }
                    /* Contratada */
                    else if (ap.get_Piso(piso).get_plaza(plaza).get_contrato() != null)
                    {
                        p.Image = global::Parking.Properties.Resources.coche_azul01;  
                        this.toolTip1.SetToolTip(p, "Piso:" + piso + "\nPlaza: " + plaza+"\nMatricula: "+ap.get_Piso(piso).get_plaza(plaza).get_vcontrato().get_matricula());            
                     
                    }
                    /* Eventual */
                    else if (ap.get_Piso(piso).get_plaza(plaza).get_ticket() != null)
                    {
                        p.Image = global::Parking.Properties.Resources.coche_rojo01;
                        this.toolTip1.SetToolTip(p, "Piso:" + piso + "\nPlaza: " + plaza + "\nMatricula: " + ap.get_Piso(piso).get_plaza(plaza).get_ticket().get_vevent().get_matricula());                       
                    }
                    /* Libre */
                    else
                    { 
                        p.Image = global::Parking.Properties.Resources.plaza_vacia01;
                    }
                   
                    p.Visible = true;                
            } // fin for
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void altaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Crear formulario modal
            Form f_altas = new f_altas(appli);
            f_altas.ShowDialog(this);
        }

        private void bajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Crear formulario modal
            Form f_bajas = new f_bajas(appli);
            f_bajas.ShowDialog(this);
        }


        private void cajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form caja = new caja(appli);

            caja.ShowDialog(this);
        }



        private void entradaSalidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form entradasalida = new entradasalida( appli);
            entradasalida.ShowDialog(this);
        }

        private void reojToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form reloj = new reloj(this.appli);
            reloj.ShowDialog(this);
        }



        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 1er Entregable con:
            //this.openFileDialog1.ShowDialog(); 

            //2do entregable con:
            FolderBrowserDialog fbd = new FolderBrowserDialog();


            if (fbd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    
                    appli.CargarSistema(fbd.SelectedPath);

                    /* Inicializar de nuevo todos los pisos con los nuevos ficheros
                     * Actualizando la ultima planta la que esta visible 
                     */
                    int pisoactual=comboBox1.SelectedIndex + 1;
                    for (int k = 1; k < 7; k++)
                    {
                        if (pisoactual != k)DibujarPlanta(k);
                    }
                    try
                    {
                        DibujarPlanta(pisoactual);
                    }
                    catch (NullReferenceException excep)
                    { /* Evitando no tener una referencia en el combobox,ya que el usuario abre directamente
                       * los fcheros antes de seleccionar nada
                       
                       */
                    }

                }
                catch (FileNotFoundException exce)
                {
                    Console.WriteLine("Esta petando file not found %s", exce);
                }
                /*catch (NullReferenceException exce)
                {
                    Console.WriteLine("Esta petando Referencia nula %s", exce);
                }
                 */
            }
        }




        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            label1_npanta_value.Visible = true;
            label1_npanta_value.Text = comboBox1.Text;

            //OBTENER INDICE DEL COMBOBOX Y SUMARLE 1 PARA EVITAR EL INDICE 0 CON LOS PISOS
            int piso = comboBox1.SelectedIndex;
            DibujarPlanta(piso + 1);
          
            this.panel1.Visible = true;
        }

        private void acercaDeMíToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Proyecto realizado por el alumno Eduardo Pablo Novella Lorente de ITIS en la Universidad Politécnica de Valencia a fecha 28/12/2011. \n\nPuedes ponerte en contacto conmigo mediante un email a la siguiente dirección: ednolo@inf.upv.es \n\nSi le gusta la aplicación puede contratar mis servicios para realizar un estudio de proyecto en su Empresa.");
        }

        private void ayudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("\nRecuerde que mediante el desplegable que tiene a su <izquierda> podrá elegir la planta a visualizar.\n\nSi deja unos instantes el puntero del ratón sobre un vehículo recibirá más información.\n\nRecuerde Introducir todos los campos cuando realize operaciones como Altas y Bajas o entradas y salidas.\n\nUsted puede dirigirse al Menú de la Aplicación <Acerca de mí> y podrá obtener mi contacto.");
        }





    }
}
