﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Parking
{
    public partial class f_bajas : Form
    {

        protected classes.aplicacion appli;

        public f_bajas(classes.aplicacion appli)
        {
            InitializeComponent();
            this.appli = appli;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string matricula = textBox1.Text.ToString();

            if (matricula == "")                
            {
                MessageBox.Show("Introduce una matrícula por favor");
                return;
            }           

            try
            {
                classes.Plaza p = appli.CrearBajaContrato(matricula);

                if (p != null)
                {
                    MessageBox.Show("Se ha dado de baja el Contrato correctamente.");
                    Form resultado_bajas = new resultado_bajas(p,appli);
                    resultado_bajas.ShowDialog(this);
                }
                else MessageBox.Show("NO se ha dado de baja el Contrato correctamente porque no existe ese Contrato.");
            }
            catch (Exception eee)
            {
                MessageBox.Show("No se ha podido dar de baja este Contrato. Vuelva a intentar a crear uno nuevo.");
                //MessageBox.Show(eee.ToString());
            }
                  
        }

       
    }
}
